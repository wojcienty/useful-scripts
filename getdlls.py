import sys
import os
import shutil

DEBUG = None;
x64 = None;

for arg in sys.argv:
	arg = str(arg);
	if arg == 'DEBUG':
		DEBUG = True;
	elif arg == 'x64':
		x64 = True;
	elif arg == 'RELEASE':
		DEBUG = False;
	elif arg == 'x86':
		x64 = False;

if DEBUG is None or x64 is None:
	raise Exception('Too few arguments!')

copyFiles = ['SysTools.dll', 'Utils.dll'];
FROM_PATH = "";
TO_PATH = "";
if DEBUG:
	FROM_PATH = "D:/Programowanie/Tools/sources/Tools/Debug/";
	TO_PATH = "D:/Programowanie/BotLearn/Debug/";
else:
	if x64:
		FROM_PATH = "D:/Programowanie/Tools/sources/Tools/x64/Release/";
		TO_PATH = "D:/Programowanie/BotLearn/x64/Release/";
	else:
		FROM_PATH = "D:/Programowanie/Tools/sources/Tools/Release/";
		TO_PATH = "D:/Programowanie/BotLearn/Release/";


for file in copyFiles:
	sourceFile = FROM_PATH + file;
	destFile = TO_PATH + file;
	if os.path.exists(destFile) == False:
		print(file + " not exists, copy fresh one");
		shutil.copy2(sourceFile, destFile);
	else:
		sourceInfo = os.stat(sourceFile);
		destInfo = os.stat(destFile);
		if sourceInfo.st_mtime > destInfo.st_mtime:
			print("Update of " + file);
			shutil.copy2(sourceFile, destFile);
		else:
			print("There is no need to update " + file);